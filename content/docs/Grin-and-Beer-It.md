---
date: 2015-07-30T12:31:19+06:00
updated-on: 2017-12-25T15:19:19+06:00
title: Grin and Beer It!!
authors: ["muniftanjim"]
categories:
  - Data visualisation
slug: beer-viz
cover:
  image: /images/grin_and_beer_it.jpg
  caption: Photo by Wil Stewart on [Unsplash](https://unsplash.com/)
  style: full
---

The **`pub capital of India`**, a moniker the city earned in the 1990s for the sheer number of pubs that sprang up in that decade, has stayed with it despite the pre-midnight closing deadline for bars. Grin and beer it, is the way of life and Win or lose, there's only one way to celebrate a match in Bangalore.

>Keep yourself updated and informed with this visual about the best pubs currently in the city. Though, there are several locations with a good rating where you can chill out without breaking the bank, but just loose your pockets a bit to enjoy the best.


### Presenting to you the best pubs in Bangalore
<img src="/images/beer-viz.png" title="Best pubs in Bangalore" />

Any favourites ?

> <strong>Note</strong>: I have used <em><a href="https://www.zomato.com" target="_blank">Zomato</a></em> for the data and it is limited to top 60 pubs in Bangalore as of June 25, 2015.
