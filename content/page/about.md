---
title: About
description: The Traveller!!
menu: main
weight: -210
---

Hi!, I'm Apoorv, I work as a Data Engineer for [SocialCops](http://socialcops.com/) in New Delhi and the blog is a place to document my work, thoughts, ideas, mistakes and in one word my journey. I hail from not-a-small-town called Jabalpur in Madhya Pradesh, and when I'm at home,I love spending my evenings just wandering near one of the many banks of river Narmada. I also volunteer with [Janwaar Castle](http://rural-changemakers.com/), aka *The Rural Change Makers*, and have learnt a lot of life lessons since my association with [Ulrike](https://twitter.com/ulrike_reinhard?lang=en) and the team in past two years. 

The blog is divided in two overall categories: 

- Life experiences
- Data and Tech 

> In the current phase of my life, I am mostly involved with working around and building data science related software. I love [R](https://www.r-project.org/about.html), the history it has been a part of, the current R community and the future it beholds.

As a software engineer I believe in the power of FOSS, and I hope to make positive efforts, useful for myself and the community, which has taught me everything I know.

The site was created using [Blogdown](https://github.com/rstudio/blogdown) and I would like to  acknowledge the efforts of [Yihui Xie](https://github.com/yihui) and team for making it so easy to create websites within the R environment
