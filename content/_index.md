---
title: Home
menu: main
weight: -270
---

#### A short bite of wisdom courtesy [Daily Zen](https://www.dailyzen.com/)


> If pressed to compare<br />This brief life,<br />I might declare:<br />It's like the boat<br />That crossed this morning's harbor,<br />Leaving no mark on the world.

> Mansei (730)
